# -*- coding: utf-8 -*-
# settings/local.py

from .base import *

DEBUG = True
ALLOWED_HOSTS = []

INSTALLED_APPS += (
    'grappelli',
    'debug_toolbar',
)

